﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhotoSharingApplication.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext() : base("PhotoAppServices"){ }
    }

    public class Login
    {
        [DisplayName("User Name")]
        [Required]
        public string UserName { get; set; }

        [DataType("Password")]
        [Required]
        public String Password { get; set; }

        [DisplayName("Remember Me?")]
        public bool RememberMe { get; set; }
    }

    public class Register
    {
        [DisplayName("User Name")]
        [Required]
        public string UserName { get; set; }

        [DataType("Password")]
        [Required]
        public String Password { get; set; }

        [DataType("Password")]
        [DisplayName("Confirm Password")]
        [Compare("Password")]
        [Required]
        public String ConfirmPassword { get; set; }

    }
}