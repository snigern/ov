﻿using System;
using System.Windows;

namespace School
{
    /// <summary>
    /// Interaction logic for StudentForm.xaml
    /// </summary>
    public partial class StudentForm : Window
    {
        #region Predefined code

        public StudentForm()
        {
            InitializeComponent();
        }

        #endregion

        // If the user clicks OK to save the Student details, validate the information that the user has provided
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Exercise 2: Task 2a: Check that the user has provided a first name
            if (string.IsNullOrEmpty(this.firstName.Text))
            {
                MessageBox.Show("The student must have a first name");
                return;
            }
            // TODO: Exercise 2: Task 2b: Check that the user has provided a last name
            if (string.IsNullOrEmpty(this.lastName.Text))
            {
                MessageBox.Show("The student must have a last name");
                return;
            }
            // TODO: Exercise 2: Task 3a: Check that the user has entered a valid date for the date of birth
            if (string.IsNullOrEmpty(this.dateOfBirth.Text) || !DateTime.TryParse(this.dateOfBirth.Text, out DateTime result))
            {
                MessageBox.Show("The date format is wrong please use mm/dd/yyyy");
                return;
            }

            // TODO: Exercise 2: Task 3b: Verify that the student is at least 5 years old
            // Save today's date.
            var today = DateTime.Today;
            var birthyear = result.Year;
            // Calculate the age.
            var age = today.Year - result.Year;
            // Go back to the year the person was born in case of a leap year
            if (age < 5)
            {
                MessageBox.Show("You are too young to join this school");
                return;
            }
                // Indicate that the data is valid
                this.DialogResult = true;
        }
    }
}
