﻿using System;
using System.IO;
using System.Text;
using FourthCoffee.DataService.Infrastructure;
using System.Net;

namespace Fourth_Coffee_Contact_Finder
{
    public class SalesDataLoader
    {
        Uri _serviceUri;
        HttpWebRequest _request;

        public SalesDataLoader(Uri serviceUri)
        {
            if(serviceUri ==null)
                 throw new NullReferenceException("serviceUri");

            this._serviceUri = serviceUri;
        }


        public SalesPerson GetPersonByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return null;

            this.InitializeRequest();

            var rawData = Encoding.Default.GetBytes(
                "{\"emailAddress\":\"" + email.Trim() + "\"}");

            this._request.Method = "POST";
            this._request.ContentType = "application/json";
            this._request.ContentLength = rawData.Length;
            

            this.WriteDataToRequestStream(rawData);

            return this.ReadDataFromResponseStream();
        }

        private void InitializeRequest()
        {
            this._request = WebRequest.Create(this._serviceUri) as HttpWebRequest;

            if (this._request == null)
                throw new NullReferenceException("_request");
        }

        private void WriteDataToRequestStream(byte[] data)
        {
            var dataStream = this._request.GetRequestStream();
            //Buffer for lykke som sender 10 byte af gangen, det nederste er uden buffer
            for (int i = 0; i < data.Length; i += 10)
            {
                dataStream.Write(data, i, data.Length < i + 10 ? data.Length - i:10);
            }
            dataStream.Close();
            //dataStream.Write(data, 0, data.Length);
        }

        private SalesPerson ReadDataFromResponseStream()
        {
            var response = this._request.GetResponse();

            if (response.ContentLength == 0)
            {
                return null;
            }

            // TODO: 08: Read and process the response data.
            var stream = new StreamReader(response.GetResponseStream());
            var result = SalesPerson.FromJson(stream.BaseStream);

            return result;
        }
    }
}
